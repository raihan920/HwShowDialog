package com.example.raihan.hwshowdialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* -- Trigger Button for showing builtin alerts start -- */
        Button myButton1 = (Button) findViewById(R.id.button1);
        myButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* -- Working with androids default alerts start -- */
                AlertDialog.Builder myAlert1 = new AlertDialog.Builder(MainActivity.this);
                myAlert1.setTitle("Confirmation!");
                myAlert1.setMessage("Are you sure you want to submit?");
                myAlert1.setCancelable(true);

                myAlert1.setPositiveButton("Ok", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(),"You pressed ok Button.", Toast.LENGTH_LONG).show();
                    }
                });

                myAlert1.setNeutralButton("Nco", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(),"You pressed Nco Button.", Toast.LENGTH_LONG).show();
                    }
                });

                myAlert1.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(),"You cancelled the dialog.", Toast.LENGTH_LONG).show();
                    }
                });

                AlertDialog showAlert1 = myAlert1.create();
                showAlert1.setCancelable(false);
                showAlert1.show();
        /* -- Working with androids default alerts end -- */
            }
        });
        /* -- Trigger Button for showing builtin alerts ends -- */

        /* -- Trigger Button for showing custom alerts start -- */
        Button myButton2 = (Button) findViewById(R.id.button2);
        myButton2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                final Dialog myDialog2 = new Dialog(MainActivity.this);
                myDialog2.setContentView(R.layout.custom_alert_layout);

                Button btn1 = (Button) myDialog2.findViewById(R.id.button1); //first button of dialog
                btn1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText et1 = (EditText) myDialog2.findViewById(R.id.editText1);
                        EditText et2 = (EditText) myDialog2.findViewById(R.id.editText2);
                        TextView tv1 = (TextView) myDialog2.findViewById(R.id.textView1);
                        String str_et1 = et1.getText().toString();
                        String str_et2 = et2.getText().toString();
                        int intVal1 = Integer.parseInt(str_et1);
                        int intVal2 = Integer.parseInt(str_et2);
                        int int_sum = intVal1 + intVal2;
                        String str_sum = String.valueOf(int_sum); //converting string to integer
                        tv1.setText(str_sum);
                    }
                });

                Button btn2 = (Button) myDialog2.findViewById(R.id.button2); //second button of dialog
                btn2.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        myDialog2.dismiss();
                        Toast.makeText(getApplicationContext(),"Summation was closed.", Toast.LENGTH_LONG).show();
                    }
                });
                myDialog2.setCancelable(false);
                myDialog2.show();
            }
        });
        /* -- Trigger Button for showing custom ends start -- */
    }
}
